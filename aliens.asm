### James Devine     ####	
### jmd149@pitt.edu  ###
 #######################
   .data
queue:		.space		256

init_color:	.word	2
init_x:		.word	32
init_y:		.word	63
newline:	.asciiz		"\n"

red:		.word		1
score:		.asciiz		"\nNumber of aliens hit is : "
shots:		.asciiz		"\nNumber of phaser firings : "
saved:		.asciiz		"\nEarth was Saved"
lost:		.asciiz		"\nEarth was Lost"
.text	

wait_to_start:
	la	$t0,0xffff0000	# status register address	
	lw	$t0,0($t0)	# read status register
	andi	$t0,$t0,1		# check for key press
	bne	$t0,$0,_keypress1
	j	wait_to_start
	
_keypress1:			#just waits for the center key to be pressed
	la	$t0,0xffff0004	# keypress register
	lw	$t0,0($t0)	# read keypress register
	subi	$t1, $t0, 66				# center key?
	beq	$t1, $0, game_start
	j	wait_to_start		# 

game_start:
	la	$s2,queue		#address pointing to the beginning of the queue
	move	$s5,$s2	
	li	$v0,30			#start the game timer
	syscall	
	move	$s0,$a0			#store the start time	
	move	$s4,$s0			#this is the time step	
	li	$s1,120000	
	li	$s3,100	
	li	$t1, 0xffff03cf		# the furthes to put an alien
	li	$t0,0xFFFF0008		# the closest to put an alien
	sub	$a1,$t1,$t0		#my range for the random number		
	li	$t1,64			#how many aliens I want
loop:	
	beq	$t1,$0,pre_poll	
	li	$v0,42			#random number
	syscall	
	li	$t2,0x03		#green LED	
	li	$t0,0xFFFF0018		#beginning of where I want to put aliens	
	add	$t0,$t0,$a0		#add the random number to the beginning address		
	lb	$t6,0($t0)	
	beq	$t6,$t2,loop		#is there alreay an alien here?
	sb	$t2,0($t0)		#store the green LED	
	subi	$t1,$t1,1		#decrement the counter of aliens
			
	j	loop
	
pre_poll:				##puts the astranaut where I want
	lw	$a0, init_x		
	lw	$a1, init_y
	lw	$a2, init_color
	jal	setLED
		
_poll:
	# check for a key press
	la	$t0,0xffff0000	# status register address	
	lw	$t0,0($t0)	# read status register
	andi	$t0,$t0,1		# check for key press
	bne	$t0,$0,_keypress
	
	inner_poll:
	subi	$sp,$sp,12
	sw	$a0,0($sp)
	sw	$a1,4($sp)
	sw	$a2,8($sp)
	
		
	li	$v0,30			#get the game time
	syscall	
	sub	$t2,$a0,$s0		#subtract that from when the game started
	bge 	$t2,$s1,game_over	#if the time = 120,000 game over	
	sub	$t2,$a0,$s4		#subtract time time step from the current time
	move	$t9,$s2
	bge	$t2,$s3,_queue		#if the time = 100 empty the queue
	
	lw	$a0,0($sp)
	lw	$a1,4($sp)
	lw	$a2,8($sp)
	addi	$sp,$sp,12
	
	j	_poll
	

_queue:
		
	jal	_remove_q				
	
	inner_queue:
	
	li	$v0,30			#get the game time
	syscall	
	move	$s4,$a0			#set this time to the time step	
	lw	$a0,0($sp)
	lw	$a1,4($sp)
	lw	$a2,8($sp)
	addi	$sp,$sp,12	
		
	j	_poll

_remove_q:
	move	$t7,$ra
	
	r_loop:
	
	li	$t5,1	
	lw	$t3,0($s5)
	li	$t0,0x100100e0		#end of my queue space
	
	beq	$s5,$t0,_size_q2			
	andi	$t4,$t3,0x000000ff
	beq	$s5,$t9,r_loop_exit	
	beq	$t4,$t5,burst_event	#is this a burst event?	
	
	inner_remove:
	sw	$0,0($s5)
	addi	$s5,$s5,4
	j	r_loop
	
	r_loop_exit:
	move	$ra,$t7
	jr	$ra

_insert_q:

	subi	$sp,$sp,4
	sw	$ra,0($sp)	
	jal	_size_q	
		
	sw	$t4,0($s2)	
	addi	$s2,$s2,4	
	lw	$ra,0($sp)
	
	addi	$sp,$sp,4
	jr	$ra
	
_size_q:
	li	$t0,0x100100e0	
	beq	$s2,$t0,re_size	
	jr	$ra
	
	re_size:
	la	$s2,queue	
	jr	$ra
	
_size_q2:
	
	la	$s5,queue	
	j	r_loop_exit
	
first_phaser:
	addi	$s6,$s6,1
	move	$t4,$0	
	subi	$a1, $a1, 1	
	lw	$a2,red		#this makes the LED just above the astronaut RED
	jal	_setLED	
	addi	$t4,$t4,1	#add 1 to identify this event as a burst	
	sll	$t3,$a0,8	#getting ready to add the x axis	
	add	$t4,$t4,$t3	#add the x axis	
	sll	$t3,$a1,16	#getting ready to add the y axis	
	add	$t4,$t4,$t3	#add the y axis	
	jal	_insert_q	
	addi	$a1,$a1,1			
	li	$a2,0x02	#keeps the astronaut orange and I maintain control of him
	jal	_setLED	
	
	j	_poll

burst_event:
	move	$t4,$0	
	
	srl	$a0,$t3,8		#get the x axis	
	andi	$a0,$a0,0x000000ff		
	srl	$a1,$t3,16		#get the y axis	
	andi	$a1,$a1,0x000000ff	
	
	beq 	$a1,$0,delete_event
	
	li	$a2,0	
	jal	setLED	
	subi	$a1,$a1,1		#decrement y axis (MOVE UP)	
	jal	_getLED			#get the LED at the new y axis	
	
	li	$t0,3	
	beq	$t2,$t0,_wave		#is the new y axis green? if so, wave	
	
	li	$a2,1

	jal	setLED
	
	addi	$t4,$t4,1

	sll	$t3,$a0,8	#getting ready to add the x axis	
	add	$t4,$t4,$t3	#add the x axis	
	sll	$t3,$a1,16	#getting ready to add the y axis	
	add	$t4,$t4,$t3	#add the y axis	
	
	jal	_insert_q		#if its not, re insert this into the queue

	j 	inner_remove
	
delete_event:		

	li	$a2,0
	jal	setLED
	j	inner_remove

_wave:
	addi	$s7,$s7,1

	li	$a2,0
	jal	setLED
	j 	inner_remove
	
	
####################################### Given	####################################	
_keypress:
	# handle a keypress to change snake direction
	la	$t0,0xffff0004	# keypress register
	lw	$t0,0($t0)	# read keypress register

	# clear current star
	li	$a2, 0
	jal 	setLED

	# center key
	subi	$t1, $t0, 66				# center key?
	beq	$t1, $0, move_done		# 

	# left key
	subi	$t1, $t0, 226				# left key?
	beq	$t1, $0, left_pressed		# 

	# right key
	subi	$t1, $t0, 227				# right key?
	beq	$t1, $0, right_pressed		# 

	# up key
	subi	$t1, $t0, 224				# up key?
	beq	$t1, $0, up_pressed			# 

	# down key
	subi	$t1, $t0, 225				# down key?
	beq	$t1, $0, down_pressed		# 

	#j	_poll
	j	inner_poll

right_pressed:
	addi	$a0, $a0, 1
	bge	$a0, 64, RIGHT_SIDE
	j	move_done
	RIGHT_SIDE:
	li	$a0, 0
	j	move_done

left_pressed:
	subi	$a0, $a0, 1

	blt	$a0, $zero, LEFT_SIDE
	j	move_done
	LEFT_SIDE:
	li	$a0, 63
	j	move_done

up_pressed:
	subi	$sp,$sp,8
	sw	$a0,0($sp)
	sw	$a1,4($sp)

	subi	$a1,$a1,1
	jal	_getLED	
	
	addi	$a1,$a1,1	
	
	lw	$a0,0($sp)
	lw	$a1,4($sp)
	addi	$sp,$sp,8
	
	li	$t0,3
	
	beq	$t2,$t0,_wave

	j	first_phaser
	DOWN_SIDE:
	li	$a1, 30
	j	first_phaser

down_pressed:
	j	game_over



move_done:
	# place code
	lw	$a2, init_color
	jal	setLED
		
j	_poll
	
setLED:
	subi	$sp, $sp, 20
	sw	$t0, 0($sp)
	sw	$t1, 4($sp)
	sw	$t2, 8($sp)
	sw	$t3, 12($sp)
	sw	$ra, 16($sp)

	jal	_setLED

	lw	$t0, 0($sp)
	lw	$t1, 4($sp)
	lw	$t2, 8($sp)
	lw	$t3, 12($sp)
	lw	$ra, 16($sp)
	addi	$sp, $sp, 20
	
	jr	$ra

_setLED:
	# range checks
	bltz	$a0,_setLED_exit
	bltz	$a1,_setLED_exit
	bge	$a0,64,_setLED_exit
	bge	$a1,64,_setLED_exit
	
	# byte offset into display = y * 16 bytes + (x / 4)
	sll	$t0,$a1,4      # y * 16 bytes
	srl	$t1,$a0,2      # x / 4
	add	$t0,$t0,$t1    # byte offset into display
	li	$t2,0xffff0008	# base address of LED display
	add	$t0,$t2,$t0    # address of byte with the LED
	# now, compute led position in the byte and the mask for it
	andi	$t1,$a0,0x3    # remainder is led position in byte
	neg	$t1,$t1        # negate position for subtraction
	addi	$t1,$t1,3      # bit positions in reverse order
	sll	$t1,$t1,1      # led is 2 bits
	# compute two masks: one to clear field, one to set new color
	li	$t2,3		
	sllv	$t2,$t2,$t1
	not	$t2,$t2        # bit mask for clearing current color
	sllv	$t1,$a2,$t1    # bit mask for setting color
	# get current LED value, set the new field, store it back to LED
	lbu	$t3,0($t0)     # read current LED value	
	and	$t3,$t3,$t2    # clear the field for the color
	or	$t3,$t3,$t1    # set color field
	sb	$t3,0($t0)     # update display
_setLED_exit:	
	jr	$ra
	

_getLED:
	# range checks
	#addi	$a1,$a1,-1
	
	bltz	$a0,_getLED_exit
	bltz	$a1,_getLED_exit
	bge	$a0,64,_getLED_exit
	bge	$a1,64,_getLED_exit
	
	# byte offset into display = y * 16 bytes + (x / 4)
	sll  $t0,$a1,4      # y * 16 bytes
	srl  $t1,$a0,2      # x / 4
	add  $t0,$t0,$t1    # byte offset into display
	la   $t2,0xffff0008
	add  $t0,$t2,$t0    # address of byte with the LED
	# now, compute bit position in the byte and the mask for it
	andi $t1,$a0,0x3    # remainder is bit position in byte
	neg  $t1,$t1        # negate position for subtraction
	addi $t1,$t1,3      # bit positions in reverse order
    	sll  $t1,$t1,1      # led is 2 bits
	# load LED value, get the desired bit in the loaded byte
	lbu  $t2,0($t0)	
	srlv $t2,$t2,$t1    # shift LED value to lsb position	
	andi $v0,$t2,0x3    # mask off any remaining upper bits
_getLED_exit:	
	jr   $ra

	
game_over:
	li	$t1,64		
	
	la	$a0,shots
	li	$v0,4
	syscall
	move	$a0,$s6
	li	$v0,1
	syscall
	la	$a0,score
	li	$v0,4
	syscall		
	
	move	$a0,$s7
	li	$v0,1
	syscall
	
	bge	$a0,$t1,earth_was_saved
	la	$a0,lost
	li	$v0,4
	syscall
	
	_end:
	
	li	$v0,10
	syscall
	
earth_was_saved:
	la	$a0,saved
	li	$v0,4
	syscall
	
	j	_end