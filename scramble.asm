###################
#James Devine  ####
#jmd149@pitt.edu #
##################

.data	

computer:	.asciiz		"computer"
project: 	.asciiz		"project"
requirement: 	.asciiz		"requirement"
description: 	.asciiz		"description"
random:		.asciiz		"random"
period:      	.asciiz		"period"
scramble:	.asciiz 	"scramble"
submit:		.asciiz		"submit"
juice:	    	.asciiz		"juice"
organization: 	.asciiz		"organization"
cheese:		.asciiz 	"cheese"
edit:		.asciiz 	"edit"

scramble_space:	.asciiz		"             "

empty:		.asciiz		" "

welcome:	.asciiz		"Welcome to Scramble!\n"
prompt:		.asciiz		"\nGuess a letter?\n"
newline:	.asciiz		"\n"
think:		.asciiz		"\nI am thinking of a word. "
word_is:	.asciiz		" The word is "
dash:		.asciiz		"_ "
score_is:		.asciiz		". Score is "
guessed_letter:	.space		15
word_length:	.space		15
temp:		.space		10
the_word:	.asciiz		"           "

another:	.asciiz		"\nDo you want to play again (y/n)?\n"
y:		.asciiz		"y"
final:		.asciiz		"\n\nYour final score is "
goodbye:	.asciiz		". Goodbye!"
correct_word:	.asciiz		"\nCorrect unscrambled word was: \n"
end:		.asciiz		"."
over:		.asciiz		"\n\nRound is over. Your final guess was: \n"
yes:		.asciiz		"Yes!"
no:		.asciiz		"No!"
.text 
	#######  $s0 has the length,never chages.$t2 holds the length, does change....$t4 holds the score
##this is the intro

	la	$a0,welcome	#load welcome message		
	li	$v0,4		#print welcome message
	syscall	
	
	la	$t0,computer	#load computer address
	
play_again:
	la	$a0,think	# I am thinking of a word....	
	li	$v0,4		#print the thinking
	syscall	

	la	$a0,word_is	
	li	$v0,4
	syscall
	
	move	$s2,$t0		#have $s2 hold random word	
	
	move	$t8,$0		#put the correct guessed counter back to zero
	
	la	$t6,the_word	#$t6 holds address to where i store the unscrambled word
	add	$s1,$t6,$0	#$s1 keeds the_word's original address
	
	la	$t7,scramble_space	#holds address of scrambled word, will increment and decrement
	move	$s4,$t7			#holds address of scrambled word, always points to the beginning
	
	add	$t4,$0,$0	#set $t4 back to zero
	add	$s0,$0,$0	#set $s0 back to zero
	
loop:
	lb	$t1,0($t0)		#load the letters, basically just counts the length
	
	beq	$t1,$0,scramble_word	#if it equals zero its time to scramble	
		
	sb	$t1,0($t7)		#store each letter into scrambled space, but not scrambled yet
		
	la	$a0,dash		#print a underscore for each letter	
	li	$v0,4
	syscall
	
	lb	$t1,0($a0)		#load the dash
	sb	$t1,0($t6)		#store the dash at the appropriate place in memory	
	
	addi	$t0,$t0,1		#increment the address
	addi	$s0,$s0,1		#keep track of length

	addi	$t4,$t4,1		#this is for the score
	addi	$t6,$t6,1		#this is for the_word address
	addi	$t7,$t7,1
	j	loop

pre_guess_again:

	move	$t6,$s1		#put the_word's address into $s1	
	move	$t7,$s4		#put address back to the beginning
	
	la	$a0,score_is		#print Score is
	li	$v0,4
	syscall

	move	$a0,$t4		#print length a.k.a score
	li	$v0,1
	syscall
	
	beq	$t4,$0,round_over #if score is zero ask if they wanna play again
	
	beq	$t8,$s0,round_over	#if the number of correctly guessed letters equals the words length
	
	move	$t2,$s0		#put the length into $t2	
	
guess_again:	
	la	$a0,prompt		#print Guess a letter
	li	$v0,4
	syscall
	
	la	$t6,the_word		#to store the word

	move	$t0,$s2		#put WORD address in $t0
	
	
	li	$v0,12			#read the guessed letter into $v0
	syscall
	
	sb	$v0,guessed_letter		#store the guessed letter
	lb	$t3,guessed_letter		#load the guessed letter
	
	lb	$s6,end			#if the user enters a ".", end the round
	beq	$s6,$t3,forfeit
	
	sb	$t2,temp		#store the length of the word
	lb	$t2,temp		#load the length of the word
	

	add	$t2,$0,$0		#start the length at zero

	la	$a0,newline
	li	$v0,4
	syscall
	
	addi	$t2,$t2,1		#increment the length of the word starting at zero
	
	

	add	$t2,$0,$0		#setting the word length to 0
	add	$t5,$0,$0

	loop2:
		beq	$t2,$s0,lose_point	#this just checks to see if i need to deduct a point
	
		lb	$t1,0($t7)	#needs to be $t7	#load the  letter from the random word (computer)**This needs to load from the scrambled word	

		beq	$t3,$t1,store_letter	#if they're equal print it, but now i need to compare it to all the letters
				
	
	back_to_loop:
		addi	$t6,$t6,1	#HOLD UP A SEC
	
		addi	$t2,$t2,1	#increment the length of the word starting at zero
		addi	$t7,$t7,1	#increment the byte we are loading
	
		j loop2
	
store_letter:
		
	sb	$t3,0($t6)	#puts the guessed letter into memory
	
	addi	$t5,$0,1	#if $t5 is 0 that i must deduct a point for a incorrect guess
	
	addi	$t8,$t8,1	#$t8 only gets incremented if u guess a correct letter
	
	beq	$t8,$s0,round_over
		
	j	back_to_loop	

	
print_guess:

	la	$a0,word_is
	li	$v0,4
	syscall
	
	move	$t6,$s1
	move	$t5,$0
	
	print:
	
		beq	$t5,$s0,pre_guess_again
	
		lb	$t1,0($t6)	#load either the letter or space at this point in memory
		move	$a0,$t1		
		li	$v0,11		#print either the letter or space
		syscall
	
		la	$a0,empty
		li	$v0,4
		syscall
	
		addi	$t5,$t5,1
		addi	$t6,$t6,1
	
		j	print

lose_point:

	beq	$t5,$0,minus_score		#is $t5 = 0?
	
	la	$a0,yes
	li	$v0,4
	syscall
	
	j 	print_guess
	
minus_score:
	
	la	$a0,no
	li	$v0,4
	syscall
	
	addi	$t4,$t4,-1	#minus the score for not guessing the right letter

	add	$t5,$0,$0	#set t5 back to zero
	
	j print_guess
	
round_over:
	add	$a0,$0,$0
	
	la	$a0,over	#Print round is over, final guess was:
	li	$v0,4
	syscall
	
	j	print_final_guess
	
	after_print_final_guess:
	
	la	$a0,correct_word	#print "Correct unscrambled word....
	li	$v0,4
	syscall
	
	move	$a0,$s2		#print the unscrambled word
	li	$v0,4
	syscall
	
	add	$s3,$s3,$t4		#$s3 holds TOTAL score	
	
	la	$a0,another	#"Do you want to play again?"
	li	$v0,4
	syscall
	li	$v0,12		# reads in either y or n
	syscall
	
	sb	$v0,temp	#store it, load it, if it equals y, play again
	lb	$t1,temp
	lb	$t2,y
	beq	$t1,$t2,random_word		

final_score:		#prints out final score before the game ends		
	
	la	$a0,final		#print "final score....
	li	$v0,4
	syscall

	move	$a0,$s3			#print the final score
	li	$v0,1
	syscall
	la	$a0,goodbye		#print goodbye
	li	$v0,4
	syscall
	
	j	exit
	
print_final_guess:
	move	$t6,$s1
	move	$t5,$0

	back:	
		beq	$t5,$s0,after_print_final_guess	#if the temp length equals the length of the word
	
		lb	$a0,0($t6)
		li	$v0,11
		syscall
		
		la	$a0,empty	#print a space between each _
		li	$v0,4
		syscall		
		
		addi	$t6,$t6,1	#increment address
		addi	$t5,$t5,1	#increment the word lenth
	
		j	back	#jump back into the loop	

forfeit:	
	move	$t4,$0
	
	j	round_over	


scramble_word:	###use either $t1,$t2,$t3,$t5
	
	addi	$t2,$0,2
	move	$a1,$s0		#puts the words length minus 1 into $t1
	addi	$a1,$a1,-1
	move	$t0,$s2		#puts original address of random word into $t0
	addi	$t7,$t7,-1	#decrememnts address of scrambled word because if not, the first char would be a zero
	
	move	$s5,$s4		#NEEED THIS
	
	scram_loop:
	
		move	$s4,$s5		#puts $s4, back to the words original address
	
		beq	$a1,$t2,pre_guess_again		
	
		li	$v0,42		#get a random number
		syscall
	
		move	$t3,$a0
	
		add	$s4,$s4,$t3
	
		lb	$t5,0($s4)	#load random index	
		
		lb	$t1,0($t7)	#load last letter of word	
		
		sb	$t1,0($s4)	#store the last letter in the first index
	
		sb	$t5,0($t7)	
	
		addi	$t7,$t7,-1	#decrememnts address of scrambled word because if not, the first char would be a zero
		addi	$a1,$a1,-1
	
		j	scram_loop
exit:
	
		li	$v0,10
		syscall

###########################################Just the words ####################################################
random_word:

		addi	$a1,$0,10	

		li	$v0,42		#get a random number
		syscall
	
		add	$t0,$a0,$0	#put random number in $t0
	
		li	$t2,0
		beq	$t0,$t2,proj
		
		li	$t2,1
		beq	$t0,$t2,req
		li	$t2,2
		beq	$t0,$t2,des
		li	$t2,3
		beq	$t0,$t2,rand
		li	$t2,4
		beq	$t0,$t2,per
		li	$t2,5
		beq	$t0,$t2,scram
		li	$t2,6
		beq	$t0,$t2,subm
		li	$t2,7
		beq	$t0,$t2,juic
		li	$t2,8
		beq	$t0,$t2,org
		li	$t2,9
		beq	$t0,$t2,che
		
		
proj:
		la	$t0,project		
		j	play_again		
	
req:
		la	$t0,requirement				
		j	play_again
des:
		la	$t0,description				
		j	play_again
rand:
		la	$t0,random				
		j	play_again
per:
		la	$t0,period				
		j	play_again
scram:
		la	$t0,scramble				
		j	play_again
subm:
		la	$t0,submit				
		j	play_again
juic:
		la	$t0,juice				
		j	play_again
org:
		la	$t0,organization	
		j	play_again
che:
		la	$t0,cheese
		j	play_again	
	
